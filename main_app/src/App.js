import {Menu} from 'antd'
import { BrowserRouter as Router, Link} from 'react-router-dom'

const menu = [{
  key: 'home',
  label: <Link to="/">主应用</Link>
}, {
  key: 'sub_app1',
  label: <Link to="/sub_app1">子应用1</Link>
}, {
  key: 'sub_app2',
  label: <Link to="/sub_app2">子应用2</Link>
}]
function App() {
  return (
    <Router>
        <Menu theme="dark" mode='inline' items={menu} />
        <div id="qiankun_container"></div>
    </Router>
  );
}

export default App;
