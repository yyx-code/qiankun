import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
// import 'antd/dist/reset.css'
import App from './App';
import reportWebVitals from './reportWebVitals';

import { registerMicroApps, start } from 'qiankun';

registerMicroApps([
  {
    name: 'sub_app1',
    entry: '//localhost:3001',
    container: '#qiankun_container',
    activeRule: '/sub_app1',
  },
  {
    name: 'sub_app2',
    entry: '//localhost:3002',
    container: '#qiankun_container',
    activeRule: '/sub_app2',
  },
]);

start();

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
